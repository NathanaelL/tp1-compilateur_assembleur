# CERIcompiler


**PLEASE**
The last version of the compiler *is not* here.
Just to be sure : send me the number of the version you will check at nathanael.lefevre@alumni.univ-avignon.fr

last version is v13 normally

YOU HAVE TO:

> git branch -a

search for the bigger version (vxx) (wher xx is a number)
exemple :
```
remotes/origin/HEAD -> origin/master
remotes/origin/TP2
remotes/origin/TP3
remotes/origin/TP3_optimisation_For_et_While
remotes/origin/TP4
remotes/origin/TP4_majOfDisplay
remotes/origin/TP6
remotes/origin/TP7
remotes/origin/Tp1
remotes/origin/master
remotes/origin/v10
remotes/origin/v11
remotes/origin/v12
remotes/origin/v13
remotes/origin/v8_cleaning
remotes/origin/v9_errorUpdate
````

-> HERE it is v13

> git checkout v13

check that the file **"AAA vxx AAA"** is not named **"AAA vxx NOT GOOD AAA"**
in this case (**"AAA vxx NOT GOOD AAA"**), 
	please consider the second last version 
	(in our exemple, it would be : git checkout v12)
	
	
	

In case of doubt, please contact me at : nathanael.lefevre@alumni.univ-avignon.fr
you can start to look at v13 but some feature could have been added in versions above
